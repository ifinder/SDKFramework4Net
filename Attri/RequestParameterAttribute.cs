﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Cn.Finder.SDKFramework.Attri
{
    /// <summary>
    /// 标记参数需要 发送到服务器端
    /// </summary>
    public class RequestParameterAttribute : Attribute
    {
        public RequestParameterAttribute()
        {
        }
        public RequestParameterAttribute(string fieldName)
        {
            FieldName = fieldName;
            OrderNumber = -1;
        }
        public RequestParameterAttribute(string fieldName,int orderNumber)
        {
            FieldName = fieldName;
            OrderNumber = orderNumber;
        }


        /// <summary>
        /// 字段的别名-请求参数名称
        /// </summary>
        public string FieldName
        {
            get;
            set;
        }

        /// <summary>
        /// 排序字段,自定义排序值区间[1-999]，没有指定 默认为-1， 越小排越靠前 
        /// </summary>
        public int OrderNumber
        {
            get;
            set;
        }
    }
}
