﻿//==============================================================
//  Create by whl at 4/15/2014 1:21:28 PM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;

namespace Cn.Finder.SDKFramework.Attri
{
    [AttributeUsage(AttributeTargets.ReturnValue | AttributeTargets.Parameter | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class JsonArrayAttribute : Attribute
    {
        private string elementName;
     

        public JsonArrayAttribute()
        {
         
        }

        public JsonArrayAttribute(string elementName)
        {
           
            this.elementName = elementName;
        }

        public string ElementName
        {
            get
            {
                if (this.elementName != null)
                {
                    return this.elementName;
                }
                return string.Empty;
            }
            set
            {
                this.elementName = value;
            }
        }

       

      

    }
}
