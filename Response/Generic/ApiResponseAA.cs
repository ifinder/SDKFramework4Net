﻿//==============================================================
//  Create by whl at 4/11/2014 11:03:59 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Cn.Finder.SDKFramework.Domain;
using Newtonsoft.Json;
using Cn.Finder.SDKFramework.Attri;

namespace Cn.Finder.SDKFramework.Response.Generic
{
    /// <summary>
    /// 返回 Response的基类
    /// </summary>
    [Serializable]
    public  class ApiResponse<T>
    {
        // Methods
        public ApiResponse()
        {
            Message = new Message();
        }


        public string Body { get; set; }




        [JsonProperty("count")]
        public int Count
        {
            get;

            set;

        }

        [JsonProperty("pageCount")]
        public int PageCount
        {
            get;
            set;
        }
        [JsonProperty("pageSize")]
        public int PageSize
        {
            get;
            set;
        }

        [JsonProperty("pageIndex")]
        public int PageIndex
        {
            get;
            set;
        }

        [JsonProperty("totalRecord")]
        public int TotalRecord
        {
            get;
            set;
        }



        [JsonProperty("message")]
        public Message Message
        {
            get;
            set;
        }
        [JsonProperty("tag")]
        public object Tag
        {
            get;
            set;
        }


        /// <summary>
        /// 数据集合
        /// </summary>
        [Cn.Finder.SDKFramework.Attri.JsonArrayAttribute("entities"), JsonArrayItemAttribute("")]
        public List<T> Entities
        {
            get;
            set;
        }


        /// <summary>
        /// 获取第一个对象
        /// </summary>
        /// <returns></returns>
        public T Single()
        {
            if (Entities != null && Entities.Count > 0)
            {
                return Entities[0];
            }
            return default(T);
        }


        /// <summary>
        /// 注意：
        /// 1. 如果返回结合是此种数据{pageIndex:2,pageSize:10,resultList:{user:[{id:1,name:\"ddd\"},{id:2,name:\"ccc\"}]}}
        /// XmlArrayItem("user")
        /// 
        /// 如果返回结合是此种数据{pageIndex:2,pageSize:10,resultList:[{id:1,name:\"ddd\"},{id:2,name:\"ccc\"}]}
        /// XmlArrayItem("") 这个空一定要加上,否则会抛出异常
        /// 
        /// 2. 返回集合类型一定要是 List   不可以是IList 否则 XML 数据解析错误
        /// </summary>
        //[XmlArray("resultList"), XmlArrayItem("")]
        //public List<User> resultList
        //{
        //    get;
        //    set;
        //}
    }


}
