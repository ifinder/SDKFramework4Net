﻿//==============================================================
//  Create by whl at 4/11/2014 11:03:59 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Cn.Finder.SDKFramework.Domain;
using Newtonsoft.Json;

namespace Cn.Finder.SDKFramework.Response
{
    /// <summary>
    /// 返回 Response的基类
    /// </summary>
    [Serializable]
    public  class ApiResponse
    {
        // Methods
        public ApiResponse()
        {
            Message = new Message();
        }

        /// <summary>
        /// 原始文本内容体
        /// </summary>
        public string Body { get; set; }



        /// <summary>
        /// 总记录数
        /// </summary>
        [Obsolete]
        [JsonProperty("count")]
        public int Count
        {
            get;

            set;

        }


        /// <summary>
        /// 总页数
        /// </summary>
        [JsonProperty("pageCount")]
        public int PageCount
        {
            get;
            set;
        }

        /// <summary>
        /// 页大小 默认为10
        /// </summary>
        [JsonProperty("pageSize")]
        public int PageSize
        {
            get;
            set;
        }

        /// <summary>
        /// 页数索引 从1开始
        /// </summary>
        [JsonProperty("pageIndex")]
        public int PageIndex
        {
            get;
            set;
        }
        /// <summary>
        /// 总记录数
        /// </summary>
        [JsonProperty("totalRecord")]
        public int TotalRecord
        {
            get;
            set;
        }


        /// <summary>
        /// 消息
        /// </summary>
        [JsonProperty("message")]
        public Message Message
        {
            get;
            set;
        }

        /// <summary>
        /// 对象
        /// </summary>
        [JsonProperty("tag")]
        public object Tag
        {
            get;
            set;
        }

        /// <summary>
        /// 注意：
        /// 1. 如果返回结合是此种数据{pageIndex:2,pageSize:10,resultList:{user:[{id:1,name:\"ddd\"},{id:2,name:\"ccc\"}]}}
        /// XmlArrayItem("user")
        /// 
        /// 如果返回结合是此种数据{pageIndex:2,pageSize:10,resultList:[{id:1,name:\"ddd\"},{id:2,name:\"ccc\"}]}
        /// XmlArrayItem("") 这个空一定要加上,否则会抛出异常
        /// 
        /// 2. 返回集合类型一定要是 List   不可以是IList 否则 XML 数据解析错误
        /// </summary>
        //[XmlArray("resultList"), XmlArrayItem("")]
        //public List<User> resultList
        //{
        //    get;
        //    set;
        //}
    }


}
