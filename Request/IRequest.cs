﻿//==============================================================
//  Create by whl at 4/11/2014 10:32:46 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cn.Finder.SDKFramework.Response;

namespace Cn.Finder.SDKFramework.Request
{
          
    public interface IRequest<T> where T : ApiResponse
    {
        
       
        // Methods

        /// <summary>
        /// 调用服务器接口名称
        /// </summary>
        /// <returns></returns>
        string GetApiName();

        /// <summary>
        /// 获取额外请求参数
        /// </summary>
        /// <returns></returns>
        IDictionary<string, string> GetParameters();

        /// <summary>
        /// 字段数据验证
        /// </summary>
        void Validate();
    }


}
