﻿//==============================================================
//  Create by whl at 4/11/2014 2:05:19 PM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cn.Finder.SDKFramework.Util;
using Cn.Finder.SDKFramework.Response;

namespace Cn.Finder.SDKFramework.Request
{
    /// <summary>
    /// 获取流数据请求
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class StreamRequest<T> : SearchRequest<T> where T : ApiResponse
    {
       
    }


}
