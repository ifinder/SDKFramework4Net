﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cn.Finder.SDKFramework.Response;
using System.Reflection;
using System.Globalization;
using System.Threading;
using Cn.Finder.SDKFramework.Attri;
using Cn.Finder.SDKFramework.Util;
using Newtonsoft.Json.Serialization;

namespace Cn.Finder.SDKFramework.Request
{
    /// <summary>
    /// 默认的查询请求基类 ， 默认支持排序功能, 区间查询时 注意排序 
    /// 
    /// 注意:所有的请求参数为 基本类型 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SearchRequest<T>: DefaultRequest<T> where T : ApiResponse
    {

        private int pageIndex = 1;

        private int pageSize = 10;


        /// <summary>
        /// 请求每页大小
        /// </summary>
        [RequestParameterAttribute(OrderNumber=1000)]
        public int PageIndex
        {
            get
            {
                return this.pageIndex;
            }
            set
            {
                this.pageIndex = value;
            }
        }

        /// <summary>
        /// 请求当前页
        /// </summary>
        [RequestParameterAttribute(OrderNumber = 1001)]
        public int PageSize
        {
            get
            {
                return this.pageSize;
            }
            set
            {
                this.pageSize = value;
            }
        }

  
        /// <summary>
        /// 排序字段 目前只支持一个字段的排序
        /// </summary>
        [RequestParameterAttribute(OrderNumber = 1002)]
        public string SortField
        {
            get;
            set;
        }


        /// <summary>
        /// 排序方式
        /// </summary>
        [RequestParameterAttribute(OrderNumber = 1003)]
        public string SortDirect
        {
            get;
            set;
        }

        /// <summary>
        /// 返回的数据不包含的字段(多个字段之间使用 逗号 隔开)
        /// </summary>
        [RequestParameterAttribute("excludeProperties", OrderNumber = 1004)]
        public string ExcludeProperties { get; set; }

        /// <summary>
        /// 返回的数据包含的字段(多个字段之间使用 逗号 隔开)
        /// </summary>
        [RequestParameterAttribute("includeProperties", OrderNumber = 1005)]
        public string IncludeProperties { get; set; }


        /// <summary>
        /// 把属性 安装OrderNumber进行排序
        /// </summary>
        /// <param name="propertyInfos"></param>
        void SortProperty(PropertyInfo[] propertyInfos)
        {

            if (propertyInfos != null && propertyInfos.Length > 0)
            {
                SortedDictionary<int, PropertyInfo> sortedDict = new SortedDictionary<int, PropertyInfo>();
                foreach (PropertyInfo property in propertyInfos)
                {
                    RequestParameterAttribute[] theAttributes = property.GetCustomAttributes(typeof(RequestParameterAttribute), false) as RequestParameterAttribute[];

                    if (theAttributes != null && theAttributes.Length > 0)
                    {
                        int orderNumber = -1;

                        RequestParameterAttribute rpa = theAttributes[0] as RequestParameterAttribute;
                       
                        orderNumber = rpa.OrderNumber;
                        sortedDict.Add(orderNumber, property);
                    }
                }

                int i=0;
                foreach(KeyValuePair<int,PropertyInfo> kvp in sortedDict)
                {
                    propertyInfos[i] = kvp.Value;
                    i++;
                }

            }
        }


        /// <summary>
        /// 默认 获取请求参数   如果参数值为null 或者字符串为 空，那么不进行提交
        /// </summary>
        /// <returns></returns>
        public override IDictionary<string, string> GetParameters()
        {

            //反射获取 请求参数字段 
            Type type = this.GetType();
            PropertyInfo[] propertyInfos = type.GetProperties();

            ///排序 --由服务器端处理 参数排序
           //  SortProperty(propertyInfos);

            if (propertyInfos != null && propertyInfos.Length > 0)
            {
                
                foreach (PropertyInfo property in propertyInfos)
                {/*
                    RequestParameterAttribute[] theAttributes = property.GetCustomAttributes(typeof(RequestParameterAttribute), false) as RequestParameterAttribute[];

                    if (theAttributes != null && theAttributes.Length > 0)
                    {
                        string propName = "";
                   
                        RequestParameterAttribute rpa = theAttributes[0] as RequestParameterAttribute;
                        if (rpa.FieldName != null && rpa.FieldName.Trim().Length > 0)
                        {
                            propName = rpa.FieldName;
                       
                        }
                        else
                        {
                            string preStr = property.Name.Substring(0, 1).ToLower();
                            propName = preStr;
                            if (property.Name.Length > 1)
                            {
                                propName += property.Name.Substring(1);
                            }
                        }
                        object propertyValue = property.GetValue(this, null);

                        if (propertyValue != null && !propertyValue.Equals(""))
                        {//如果属性值 为 null 或者空 ""， 不会发送此参数
                            Type propertyType = property.PropertyType;
                            if (propertyType.ToString().Equals("System.String"))
                            {
                                if (propertyValue.ToString().Trim().Length > 0)
                                {
                                    AddParameter(propName, propertyValue.ToString());
                                }
                               
                            }
                            else if (propertyValue is DateTime)
                            {
                                DateTime pv = (DateTime)propertyValue;

                                propertyValue = pv.ToString(Constants.DATE_FORMAT);
                            }
                            else
                            {
                                AddParameter(propName, propertyValue.ToString());
                            }
                        }
                    }*/

                    Newtonsoft.Json.JsonIgnoreAttribute[] jsonIgnoreAttributes = property.GetCustomAttributes(typeof(Newtonsoft.Json.JsonIgnoreAttribute), false) as Newtonsoft.Json.JsonIgnoreAttribute[];

                    if (jsonIgnoreAttributes != null && jsonIgnoreAttributes.Length > 0)
                    {
                        continue;
                    }

                    Newtonsoft.Json.JsonPropertyAttribute[] theAttributes = property.GetCustomAttributes(typeof(Newtonsoft.Json.JsonPropertyAttribute), false) as Newtonsoft.Json.JsonPropertyAttribute[];

                    string propName = "";
                    if (theAttributes == null || theAttributes.Length == 0)
                    {
                        string preStr = property.Name.Substring(0, 1).ToLower();
                        propName = preStr;
                        if (property.Name.Length > 1)
                        {
                            propName += property.Name.Substring(1);
                        }

                    }


                    else
                    {


                        Newtonsoft.Json.JsonPropertyAttribute rpa = theAttributes[0] as Newtonsoft.Json.JsonPropertyAttribute;
                        if (rpa.PropertyName != null && rpa.PropertyName.Trim().Length > 0)
                        {
                            propName = rpa.PropertyName;

                        }
                    }


                    object propertyValue = property.GetValue(this, null);

                    if (propertyValue is EnumObject<string>)
                    {
                        propertyValue = (propertyValue as EnumObject<string>).ToString();
                    }

                    else if (propertyValue is EnumObject<int>)
                    {
                        propertyValue = (propertyValue as EnumObject<int>).ToString();
                    }
                    else if (propertyValue is EnumObject<float>)
                    {
                        propertyValue = (propertyValue as EnumObject<float>).ToString();
                    }
                    else if (propertyValue is EnumObject<double>)
                    {
                        propertyValue = (propertyValue as EnumObject<double>).ToString();
                    }

                    if (propertyValue != null && !propertyValue.Equals(""))
                    {//如果属性值 为 null 或者空 ""， 不会发送此参数
                        Type propertyType = property.PropertyType;
                       // if (propertyType.ToString().Equals("System.String"))
                      // {
                            if (propertyValue.ToString().Trim().Length > 0)
                            {
                                if (propertyValue is DateTime)
                                {
                                    DateTime pv = (DateTime)propertyValue;

                                    propertyValue = pv.ToString(Constants.DATE_FORMAT);

                                   // AddParameter(propName, propertyValue.ToString());
                                }

                                AddParameter(propName, propertyValue.ToString());
                            }

                      //  }
                      /*  else if (propertyValue is DateTime)
                        {
                            DateTime pv = (DateTime)propertyValue;

                            propertyValue = pv.ToString(Constants.DATE_FORMAT);
                       
                            AddParameter(propName, propertyValue.ToString());
                        }*/

                    }
                }
            }


            return parameters;
        }
    }
}
