﻿//==============================================================
//  Create by whl at 4/11/2014 2:05:19 PM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cn.Finder.SDKFramework.Util;
using Cn.Finder.SDKFramework.Response;

namespace Cn.Finder.SDKFramework.Request
{
    /// <summary>
    /// 文件上传接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IUploadRequest<T> : IRequest<T> where T : ApiResponse
    {
        // Methods
        IDictionary<string, FileItem> GetFileParameters();
    }


}
