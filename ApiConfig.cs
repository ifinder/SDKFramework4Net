﻿//==============================================================
//  Create by whl at 1/6/2015 3:43:35 PM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cn.Finder.SDKFramework
{
    /// <summary>
    /// 全局配置
    /// </summary>
    public class ApiConfig
    {
        /// <summary>
        ///  接口服务参数配置, 应在应用启动加载
        /// </summary>
        public static class ServiceInterfaceConfig
        {
            public static string AppKey = "testKey";

            public static string AppSecret = "testSecret";

            /// <summary>
            /// 应用根目录 http://xxx.xxx.xxx.xxx:80/APS
            /// </summary>
            public static string ContextRootUrl {
                
                get;
                set;
            }

            /// <summary>
            /// 普通请求服务地址
            /// </summary>
            public static string AuthInterfaceUrl{
                get{
                    return ContextRootUrl + "/service/rest/auth/login";
                    
                }
            }

            /// <summary>
            /// 普通请求服务地址
            /// </summary>
            public static string ServiceInterfaceUrl{
                 get {
                     return  ContextRootUrl + "/service/rest/interface";
                 }
            }

            /// <summary>
            /// 下载文件数据服务地址
            /// </summary>
            public static string ServiceInterfaceStreamUrl {
                get{
                    return ContextRootUrl + "/service/rest/stream_interface";
                }
            }


            /// <summary>
            /// 上传文件数据服务地址
            /// </summary>
            public static string ServiceInterfaceUploadStreamUrl {
                get{
                    return ContextRootUrl + "/service/rest/upload_interface";
                }
            }



           


            /// <summary>
            /// 
            /// </summary>
            /// <param name="url">被包装的服务地址</param>
            /// <param name="includeProperties">包含序列化的属性</param>
            /// <param name="excludeProperties"> 不包含序列化属性 如不包含 fields -》 new string[]{"fields"}</param>
            /// <returns></returns>
            public static string GetUrl(string url, string[] includeProperties, string[] excludeProperties)
            {
                StringBuilder sb_url = new StringBuilder();
                if (excludeProperties != null && excludeProperties.Length > 0)
                {

                    sb_url.Append("?excludeProperties=");
                    foreach (string item in excludeProperties)
                    {
                        sb_url.Append(item).Append(",");
                    }
                    sb_url.Remove(sb_url.Length - 1, 1);
                }



                return sb_url.ToString();

            }


            /// <summary>
            /// 获取主机 或者IP地址
            /// </summary>
            public static string Host
            {
                get
                {

                    string url = ContextRootUrl.Clone().ToString().ToLower();

                    string url_tmp = url.Replace("http://", "");
                    url_tmp += "/";
                    url_tmp = url_tmp.Substring(0, url_tmp.IndexOf("/"));

                    string[] datas = url_tmp.Split(new char[] { ':' });

                  
                    return datas[0];

                }
            }


            /// <summary>
            /// 获取端口
            /// </summary>
            public  static int Port
            {
                get
                {

                    string url = ContextRootUrl.Clone().ToString().ToLower();

                    string url_tmp= url.Replace("http://", "");
                    url_tmp += "/";
                    url_tmp = url_tmp.Substring(0, url_tmp.IndexOf("/"));

                    string[] datas = url_tmp.Split(new char[] { ':'});

                    try
                    {
                        return int.Parse(datas[1]);
                    }
                    catch
                    {
                        return 80;
                    }

                  
                }
            }






        }
    }
}
