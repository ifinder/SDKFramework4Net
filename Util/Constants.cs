﻿//==============================================================
//  Create by whl at 6/12/2014 1:41:06 PM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cn.Finder.SDKFramework.Util
{
    public class Constants
    {
        /// <summary>
        /// 时间格式
        /// </summary>
        public const  string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.fff";
    }
}
