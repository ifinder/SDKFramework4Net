﻿//==============================================================
//  Create by whl at 4/11/2014 10:54:38 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Cn.Finder.SDKFramework.Util
{
    /// <summary>
    /// 请求参数验证
    /// </summary>
    public sealed class RequestValidator
    {
        /// <summary>
        /// 错误编码： 参数无效
        /// </summary>
        private const string ERR_CODE_PARAM_INVALID = "41"; 

        /// <summary>
        /// 错误编码： 参数缺失
        /// </summary>
        private const string ERR_CODE_PARAM_MISSING = "40";


        /// <summary>
        /// 错误消息： 参数无效
        /// </summary>
        private const string ERR_MSG_PARAM_INVALID = "client-error:Invalid arguments:{0}";

        /// <summary>
        /// 错误消息： 参数缺失
        /// </summary>
        private const string ERR_MSG_PARAM_MISSING = "client-error:Missing required arguments:{0}";


        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">参数名称</param>
        /// <param name="value">参数值</param>
        /// <param name="maxLength">最大长度</param>
        public static void ValidateMaxLength(string name, string value, int maxLength)
        {
            if ((value != null) && (value.Length > maxLength))
            {
                throw new ApiException(ERR_CODE_PARAM_INVALID, string.Format("client-error:Invalid arguments:{0}", name));
            }
        }


        /// <summary>
        /// 验证 文件最大长度
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        public static void ValidateMaxLength(string name, FileItem value, int maxLength)
        {
            if (((value != null) && (value.GetContent() != null)) && (value.GetContent().Length > maxLength))
            {
                throw new ApiException(ERR_CODE_PARAM_INVALID, string.Format("client-error:Invalid arguments:{0}", name));
            }
        }


        /// <summary>
        /// 验证 分隔后 数组长度
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value">逗号分隔的字符串</param>
        /// <param name="maxSize"></param>
        public static void ValidateMaxListSize(string name, string value, int maxSize)
        {
            if (value != null)
            {
                char[] separator = new char[] { ',' };
                string[] strArray = value.Split(separator);
                if ((strArray != null) && (strArray.Length > maxSize))
                {
                    throw new ApiException(ERR_CODE_PARAM_INVALID, string.Format("client-error:Invalid arguments:{0}", name));
                }
            }
        }


        /// <summary>
        /// 验证 value的最大值不能超过 maxValue
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="maxValue"></param>
        public static void ValidateMaxValue(string name, long? value, long maxValue)
        {
            if (value.HasValue && (value.HasValue && (value.Value > maxValue)))
            {
                throw new ApiException(ERR_CODE_PARAM_INVALID, string.Format("client-error:Invalid arguments:{0}", name));
            }
        }


        /// <summary>
        /// 验证 value的最小长度 不能小于 minLength
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="minLength"></param>
        public static void ValidateMinLength(string name, string value, int minLength)
        {
            if ((value != null) && (value.Length < minLength))
            {
                throw new ApiException(ERR_CODE_PARAM_INVALID, string.Format("client-error:Invalid arguments:{0}", name));
            }
        }


        /// <summary>
        /// 验证 value的最小值 不能小于 minValue
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="minValue"></param>
        public static void ValidateMinValue(string name, long? value, long minValue)
        {
            if (value.HasValue && (value.HasValue && (value.Value < minValue)))
            {
                throw new ApiException(ERR_CODE_PARAM_INVALID, string.Format("client-error:Invalid arguments:{0}", name));
            }
        }


        /// <summary>
        /// 验证 字段值不能为空
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public static void ValidateRequired(string name, object value)
        {
            if (value == null)
            {
                throw new ApiException(ERR_MSG_PARAM_INVALID, string.Format("client-error:Missing required arguments:{0}", name));
            }
            if (value.GetType() == typeof(string))
            {
                string str = value as string;
                if (string.IsNullOrEmpty(str))
                {
                    throw new ApiException(ERR_MSG_PARAM_INVALID, string.Format("client-error:Missing required arguments:{0}", name));
                }
            }

            else if (value is IList)
            {
                IList list = value as IList;
                if (list.Count==0)
                {
                    throw new ApiException(ERR_MSG_PARAM_INVALID, string.Format("client-error:Collection Count is 0 :{0}", name));
                }
            }
        }
    }
}
