﻿//==============================================================
//  Create by whl at 4/11/2014 10:54:12 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;

using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;
using System.IO;
using WebSocketSharp.Net;

namespace Cn.Finder.SDKFramework.Util
{

    public sealed class WebUtils
    {
        private int _timeout = 10000;

        public string BuildGetUrl(string url, IDictionary<string, string> parameters)
        {
            if ((parameters != null) && (parameters.Count > 0))
            {
                if (url.Contains("?"))
                {
                    url = url + "&" + BuildQuery(parameters);
                    return url;
                }
                url = url + "?" + BuildQuery(parameters);
            }
            return url;
        }

        public static string BuildQuery(IDictionary<string, string> parameters)
        {
            StringBuilder builder = new StringBuilder();
            bool flag = false;
            IEnumerator<KeyValuePair<string, string>> enumerator = parameters.GetEnumerator();
            while (enumerator.MoveNext())
            {
                KeyValuePair<string, string> current = enumerator.Current;
                string key = current.Key;
                string value = current.Value;

                //if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                if (!string.IsNullOrEmpty(key))
                {
                    if (flag)
                    {
                        builder.Append("&");
                    }
                    builder.Append(key);
                    builder.Append("=");

                    builder.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                    flag = true;
                }
            }
            return builder.ToString();
        }

        public bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        public string DoGet(string url, IDictionary<string, string> parameters)
        {
            if ((parameters != null) && (parameters.Count > 0))
            {
                if (url.Contains("?"))
                {
                    url = url + "&" + BuildQuery(parameters);
                }
                else
                {
                    url = url + "?" + BuildQuery(parameters);
                }
            }
            HttpWebRequest webRequest = this.GetWebRequest(url, "GET");
            webRequest.ContentType = "application/x-www-form-urlencoded;charset=utf-8";

            //设置数据返回格式
            string format = "";
            parameters.TryGetValue("format", out format);
            webRequest.Accept = "application/" + format;

            HttpWebResponse rsp = (HttpWebResponse)webRequest.GetResponse();
            Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
            return this.GetResponseAsString(rsp, encoding);
        }

        public string DoPost(string url, IDictionary<string, string> parameters)
        {
            HttpWebRequest webRequest = this.GetWebRequest(url, "POST");
            webRequest.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            webRequest.Timeout = 10000; //超市设置10秒钟
            
  
            //设置数据返回格式
            string format = "";
            parameters.TryGetValue("format", out format);
            webRequest.Accept = "application/" + format;

            byte[] bytes = Encoding.UTF8.GetBytes(BuildQuery(parameters));
            Stream requestStream = webRequest.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse rsp = (HttpWebResponse)webRequest.GetResponse();
            string charSet = "UTF-8";
            if (rsp.CharacterSet != null && rsp.CharacterSet != "")
            {
               charSet = rsp.CharacterSet;
            }
            Encoding encoding = Encoding.GetEncoding(charSet);
            return this.GetResponseAsString(rsp, encoding);
        }


        /// <summary>
        /// 发送Post请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="textParams"></param>
        /// <param name="fileParams"></param>
        /// <returns></returns>
        public string DoPost(string url, IDictionary<string, string> textParams, IDictionary<string, FileItem> fileParams)
        {
           

            
            if ((fileParams == null) || (fileParams.Count == 0))
            {
                return this.DoPost(url, textParams);
            }
            string str = DateTime.Now.Ticks.ToString("X");
            string MULTIPART_FROM_DATA = "Multipart/form-data";
            string PREFIX = "--", LINEND = "\r\n";
            string CHARSET = "UTF-8";
            byte[] BOUNDARY = Encoding.UTF8.GetBytes("\r\n--" + str + "\r\n");
            byte[] BOUNDARY_END = Encoding.UTF8.GetBytes("\r\n--" + str + "--\r\n");
            byte[] LINEND_BYTE = Encoding.UTF8.GetBytes(LINEND);



            HttpWebRequest webRequest = this.GetWebRequest(url, "POST");
            webRequest.ContentType = "multipart/form-data;charset=utf-8;boundary=" + str;

            webRequest.Timeout = 100000;

            Stream requestStream = webRequest.GetRequestStream();
           
            string format = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            IEnumerator<KeyValuePair<string, string>> enumerator = textParams.GetEnumerator();
            while (enumerator.MoveNext())
            {
                KeyValuePair<string, string> current = enumerator.Current;
              
                string s = string.Format(format, current.Key, current.Value);
                byte[] buffer3 = Encoding.UTF8.GetBytes(s);
                requestStream.Write(BOUNDARY, 0, BOUNDARY.Length);
                requestStream.Write(buffer3, 0, buffer3.Length);
            
            }
            
           
            
            string str4 = "Content-Disposition: form-data;name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            IEnumerator<KeyValuePair<string, FileItem>> enumerator2 = fileParams.GetEnumerator();
            while (enumerator2.MoveNext())
            {
                KeyValuePair<string, FileItem> pair3 = enumerator2.Current;
                string key = pair3.Key;
                FileItem item = pair3.Value;
               // string str6 = string.Format(str4, key, item.GetFileName(), item.GetMimeType());
                string str6 = string.Format(str4, key, item.GetFileName(), item.GetMimeType());//"application/octet-stream"




                byte[] buffer4 = Encoding.UTF8.GetBytes(str6);

                requestStream.Write(BOUNDARY, 0, BOUNDARY.Length);

                requestStream.Write(buffer4, 0, buffer4.Length);
                byte[] content = item.GetContent();
                requestStream.Write(content, 0, content.Length);

                requestStream.Write(LINEND_BYTE, 0, LINEND_BYTE.Length);

            }
            requestStream.Write(LINEND_BYTE, 0, LINEND_BYTE.Length);
            requestStream.Write(BOUNDARY_END, 0, BOUNDARY_END.Length);
            requestStream.Flush();
            requestStream.Close();
            HttpWebResponse rsp = (HttpWebResponse)webRequest.GetResponse();
            Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
            return this.GetResponseAsString(rsp, encoding);










        }

        /// <summary>
        /// 读取响应流到字符串中
        /// </summary>
        /// <param name="rsp"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public string GetResponseAsString(HttpWebResponse rsp, Encoding encoding)
        {
            Stream responseStream = null;
            StreamReader reader = null;
            string resultStr;
            try
            {
                responseStream = rsp.GetResponseStream();
                reader = new StreamReader(responseStream, encoding);
                resultStr = reader.ReadToEnd();
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (responseStream != null)
                {
                    responseStream.Close();
                }
                if (rsp != null)
                {
                    rsp.Close();
                }
            }
            return resultStr;
        }

        public HttpWebRequest GetWebRequest(string url, string method)
        {
            HttpWebRequest request = null;
            if (url.Contains("https"))
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(this.CheckValidationResult);
                request = (HttpWebRequest)WebRequest.CreateDefault(new Uri(url));
            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create(url);
            }
            request.ServicePoint.Expect100Continue = false;
            request.Method = method;
            request.KeepAlive = true;
            request.UserAgent = "Aps4Net";
            request.Timeout = this._timeout;
            return request;
        }

        public int Timeout
        {
            get
            {
                return this._timeout;
            }
            set
            {
                this._timeout = value;
            }
        }
    }
}
