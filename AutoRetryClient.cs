﻿//==============================================================
//  Create by whl at 4/11/2014 11:02:19 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cn.Finder.SDKFramework.Request;
using System.Threading;
using Cn.Finder.SDKFramework.Response;

namespace Cn.Finder.SDKFramework
{
    public class AutoRetryClient : DefaultClient
    {
        // Fields
        private int maxRetryCount;
        private static readonly ApiException RETRY_FAIL;
        [ThreadStatic]
        private static int retryCounter;
        private int retryWaitTime;
        private bool throwIfOverMaxRetry;

        // Methods
        static AutoRetryClient()
        {
            RETRY_FAIL = new ApiException("sdk.retry-call-fail", "API调用重试失败");
            retryCounter = -1;

        }

        public AutoRetryClient(string serverUrl, string appKey, string appSecret)
            : base(serverUrl, appKey, appSecret)
        {
            this.maxRetryCount = 3;
            this.retryWaitTime = 500;

        }
        public AutoRetryClient(string serverUrl, string appKey, string appSecret, string format)
            : base(serverUrl, appKey, appSecret, format)
        {
            this.maxRetryCount = 3;
            this.retryWaitTime = 500;

        }
        public new T Execute<T>(IRequest<T> request) where T : ApiResponse
        {
            return this.Execute<T>(request, null);

        }


        public new T Execute<T>(IRequest<T> request, string session) where T : ApiResponse
        {
            return this.Execute<T>(request, session, DateTime.Now);

        }


        public new T Execute<T>(IRequest<T> request, string session, DateTime timestamp) where T : ApiResponse
        {
            T local = null;
            //try
            //{
            //    retryCounter++;
            //    local = base.Execute<T>(request, session, timestamp);
            //    if (!local.IsError)
            //    {
            //        return local;
            //    }
            //    if (retryCounter < this.maxRetryCount)
            //    {
            //        if ((local.SubErrCode == null) || !local.SubErrCode.StartsWith("isp."))
            //        {
            //            return local;
            //        }
            //        Thread.Sleep(this.retryWaitTime);
            //        return this.Execute<T>(request, session, timestamp);
            //    }
            //    if (this.throwIfOverMaxRetry)
            //    {
            //        throw RETRY_FAIL;
            //    }
            //}
            //catch (Exception exception)
            //{
            //    if ((exception != RETRY_FAIL) && (retryCounter < this.maxRetryCount))
            //    {
            //        Thread.Sleep(this.retryWaitTime);
            //        return this.Execute<T>(request, session, timestamp);
            //    }
            //}
            //finally
            //{
            //    this.retryWaitTime = -1;
            //}
            return local;


        }
        public void SetMaxRetryCount(int maxRetryCount)
        {
            this.maxRetryCount = maxRetryCount;
        }

        public void SetRetryWaitTime(int retryWaitTime)
        {
            this.retryWaitTime = retryWaitTime;

        }
        public void SetThrowIfOverMaxRetry(bool throwIfOverMaxRetry)
        {
            this.throwIfOverMaxRetry = throwIfOverMaxRetry;

        }
    }

 

}
