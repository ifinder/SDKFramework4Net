﻿//==============================================================
//  Create by whl at 4/11/2014 11:00:57 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Cn.Finder.SDKFramework
{
    [Serializable]
    public abstract class ApiObject:INotifyPropertyChanged
    {
        // Methods
        protected ApiObject()
        {
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }


}
