﻿//==============================================================
//  Create by whl at 4/11/2014 11:03:02 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cn.Finder.SDKFramework.Request;
using Cn.Finder.SDKFramework.Response;
using Cn.Finder.SDKFramework.Util;
using Cn.Finder.SDKFramework.Response.Generic;

namespace Cn.Finder.SDKFramework
{

    // public delegate void RequestExecutorCallbackDelegate<T>(IRequest<T> request) where T : ApiResponse;

    ////定义回调委托
    //public delegate void ResponseCallbackDelegate(ApiResponse apiResponse);


    public interface IClient
    {

        //定位响应事件
        //event ResponseCallbackDelegate OnResponse;


        // Methods
        /// <summary>
        /// 发送客户端请求
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        T Execute<T>(IRequest<T> request) where T : ApiResponse;


        /// <summary>
        /// 发送客户端请求
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        T Execute<T>(IRequest<T> request, string session) where T : ApiResponse;

        /// <summary>
        /// 发送请求，并返回Response对象 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="session"></param>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        T Execute<T>(IRequest<T> request, string session, DateTime timestamp) where T : ApiResponse;



        //// Methods
        //ApiResponse<T> Execute<T>(IRequest<ApiResponse<T>> request) where T : ApiObject;
        //ApiResponse<T> Execute<T>(IRequest<ApiResponse<T>> request, string session) where T : ApiObject;

        ///// <summary>
        ///// 发送请求，并返回Response对象 
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="request"></param>
        ///// <param name="session"></param>
        ///// <param name="timestamp"></param>
        ///// <returns></returns>
        //ApiResponse<T> Execute<T>(IRequest<ApiResponse<T>> request, string session, DateTime timestamp) where T : ApiObject;





        //void ExecuteInThread<T>(IRequest<T> request) where T : ApiResponse;

        //纯文本数据请求
        string Execute(ApiDictionary parameters, string session, DateTime timestamp,string method);

        //带有文件数据
        string Execute(ApiDictionary parameters, IDictionary<string,FileItem> fileParams, string session, DateTime timestamp, string method);


        //纯文本数据请求构建请求字符串
        string BuildReqGetUrlString(ApiDictionary parameters, string session, DateTime timestamp, string method);


        /// <summary>
        /// 上传文件测试
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="session"></param>
        /// <param name="timestamp"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        string ExecuteFileStream(ApiDictionary parameters, string session, DateTime timestamp, string method);
    }
}
