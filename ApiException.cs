﻿//==============================================================
//  Create by whl at 4/11/2014 11:03:45 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cn.Finder.SDKFramework
{
    public class ApiException : Exception
    {
    

        // Methods
        public ApiException()
        {
        }
        public ApiException(string message)
            : base(message)
        {
        }

        protected ApiException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        public ApiException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        public ApiException(string errorCode, string errorMsg)
            : base(errorCode + ":" + errorMsg)
        {
            ErrorCode = errorCode;
            ErrorMsg = errorMsg;
        }

        // Properties
        public string ErrorCode { get; set; }
        public string ErrorMsg { get; set; }
    }

}