﻿//==============================================================
//  Create by whl at 4/11/2014 11:02:45 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Cn.Finder.SDKFramework
{
    public class DefaultLogger : ILogger
    {
        // Fields
        public const string DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public const string LOG_FILE_NAME = "sdk.log";

        // Methods
        static DefaultLogger()
        {
            try
            {
                Trace.Listeners.Add(new TextWriterTraceListener(LOG_FILE_NAME));
            }
            catch (Exception)
            {
                Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            }
            Trace.AutoFlush = true;

        }
        public DefaultLogger()
        {
        }
        public void Error(string message)
        {
            Trace.WriteLine(message, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " ERROR");
        }
        public void Info(string message)
        {
            Trace.WriteLine(message, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " INFO");
        }
        public void Warn(string message)
        {
            Trace.WriteLine(message, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " WARN");
        }
    }

 

}
