﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Cn.Finder.SDKFramework.Domain
{
    /// <summary>
    /// 响应消息
    /// </summary>
    [Serializable]
    public class Message:ApiObject
    {
        /// <summary>
        /// 状态码-成功
        /// </summary>
        public  const   int StatusCode_OK=200; 

        /// <summary>
        /// 客户端错误 
        /// </summary>
        public const int StatusCode_ClientError = 403;

        /// <summary>
        /// 服务器未连接上 
        /// </summary>
        public const int StatusCode_ServerNotFound = 404;

        /// <summary>
        /// 服务器端错误
        /// </summary>
        public const int StatusCode_ServerError = 501;

        /// <summary>
        /// 接口名称不存在
        /// </summary>
        public const int StatusCode_ApiNameError = 501;

        /// <summary>
        /// 接口被禁用
        /// </summary>
        public static int StatusCode_Disabled = 800;
        /// <summary>
        /// 处理不一定成功，返回提示客户端信息
        /// </summary>
        public static int StatusCode_InfoExp = 801;

        public Message()
        {
            StatusCode = StatusCode_OK;
        }


        [XmlElement("statusCode")]
        public int StatusCode
        {
            get;
            set;
        }

     
         [XmlElement("msg")]
        public String Msg
        {
            get;
            set;
        }


         [XmlElement("detail")]
         public String Detail
         {
             get;
             set;
         }


         public int ResultCode
         {
             get;
             set;
         }
         public String ResultMsg
         {
             get;
             set;
         }

    }
}
