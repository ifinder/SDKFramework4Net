﻿1. 查询请求 需要继承 SearchRequest
2. 查询以外的请求（增加、修改、删除） 继承 DefaultRequest
3. 所有的 返回响应 对象 继承 ApiResponse
4. 对于 区间类型的搜索， 如 查询 开始日期 - 结束日期  。
   药品开始和结束日期作为搜索条件都不传递，要么 2个都要传递， 如果页面上没有输入结束日期值
   那么可以写默认 最大日期值 如 9999-1-1 类似这样的 需求
5. 如需要使用内置的多线程请求 可以 继承 RequestTemplateExecuter，然后实现相应的方法
6. IRequest 中定义的属性 默认都是请求参数, 如果不需要那么使用JsonIgnore标记 , 里面的参数一般 首字母小写,需要和返回JSON 键 名称一样（注意大小写） 
	修改参数传递的名称 标注是 JsonProperty("xxxx")
7. 返回 属性 如果是集合
		JSON类型返回 需要加上 [JsonArrayAttribute("entities"), JsonArrayItemAttribute("")]
		XML数据返回 加上 [XmlArray("entities"), XmlArrayItem("user")]   XmlArrayItem为 项的名称

8. 请求 后 需要判断是否成功 ：  resp.Message.StatusCode == Cn.Finder.SDKFramework.Domain.Message.StatusCode_OK
     然后再使用业务

9. 对于表、视图的查询，请求的参数 需要在结果集返回的时候一起返回字段
10. 对于 调用的 是 存储过程， 请求参数的 顺序 一定要 和存储过程参数的顺序 一致，  并且存储过程中最后2个参数一定要加上
    2个整型的参数来接收当前页 索引 和也大小 。 一般默认  @p_pageIndex int, @p_pageSize int

11. 在配置完 接口、表配置或者列配置，要想及时生效，需要刷新缓存
12. 接口部分提供字段的类型 如 等于操作符，  区间操作符 不等于操作符，  做不同的比较 请配置不同操作符
    IN 操作符 的时候   值使用 逗号（，） 隔开
13.编程规范：  接口部分创建的 视图、存储过程的名称应该是 aps_api_v  aps_api_p   数据库_api_类型_



14. 根据日期进行查询的时候，请求日期参数 定义为 字符串类型， 列配置可以设置成日期类型