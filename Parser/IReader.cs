﻿//==============================================================
//  Create by whl at 4/11/2014 10:41:38 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Cn.Finder.SDKFramework.Parser
{
    public interface IReader
    {
        // Methods
        IList GetListObjects(string listName, string itemName, Type type, DConvert convert);


        /// <summary>
        /// 获取 属性的原生值
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        object GetPrimitiveObject(object name);

        /// <summary>
        /// 获取 属性的 对象值
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="convert"></param>
        /// <returns></returns>
        object GetReferenceObject(object name, Type type, DConvert convert);


        /// <summary>
        /// 返回JSON结果中是否包含指定的字段
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool HasReturnField(object name);

    }
}
