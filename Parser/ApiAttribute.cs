﻿//==============================================================
//  Create by whl at 4/11/2014 10:42:07 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Cn.Finder.SDKFramework.Parser
{
    public  class ApiAttribute
    {
           // Methods
        public ApiAttribute()
        {
        }

        // Properties
        public string ItemName { get; set; }
        public Type ItemType { get; set; }
        public string ListName { get; set; }
        public Type ListType { get; set; }
        public MethodInfo Method { get; set; }

    }
}
