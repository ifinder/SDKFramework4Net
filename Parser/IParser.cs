﻿//==============================================================
//  Create by whl at 4/11/2014 10:38:16 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cn.Finder.SDKFramework.Response;

namespace Cn.Finder.SDKFramework.Parser
{
    interface IParser
    {
        T Parse<T>(string body) where T : ApiResponse;

    }
}
