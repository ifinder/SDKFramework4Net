﻿//==============================================================
//  Create by whl at 4/11/2014 10:45:28 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Jayrock.Json;

namespace Cn.Finder.SDKFramework.Parser
{
    public class JsonReader : IReader
    {
        // Fields
        private IDictionary json;

        public JsonReader(IDictionary json)
        {
            this.json = json;
        }

        public IList GetListObjects(string listName, string itemName, Type type, DConvert convert)
        {
            IList list = null;

            if (itemName != "")
            {


                IDictionary dictionary = this.json[listName] as IDictionary;
                if ((dictionary != null) && (dictionary.Count > 0))
                {
                    IList list2 = dictionary[itemName] as IList;
                    if ((list2 == null) || (list2.Count <= 0))
                    {
                        return list;
                    }
                    Type[] typeArguments = new Type[] { type };
                    list = Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArguments)) as IList;
                    IEnumerator enumerator = list2.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            object current = enumerator.Current;
                            if (typeof(IDictionary).IsAssignableFrom(current.GetType()))
                            {
                                IDictionary json = current as IDictionary;
                                object obj3 = convert(new JsonReader(json), type);
                                if (obj3 != null)
                                {
                                    list.Add(obj3);
                                }
                            }
                            else if (!typeof(IList).IsAssignableFrom(current.GetType()))
                            {
                                if (typeof(JsonNumber).IsAssignableFrom(current.GetType()))
                                {
                                    list.Add(((JsonNumber)current).ToInt64());
                                    continue;
                                }
                                list.Add(current);
                            }
                        }
                    }
                    finally
                    {
                        IDisposable disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                }
            }
            else
            {
                IList list2 = this.json[listName] as IList;
                if ((list2 == null) || (list2.Count <= 0))
                {
                    return list;
                }
                Type[] typeArguments = new Type[] { type };
                list = Activator.CreateInstance(typeof(List<>).MakeGenericType(typeArguments)) as IList;
                IEnumerator enumerator = list2.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        object current = enumerator.Current;
                        if (typeof(IDictionary).IsAssignableFrom(current.GetType()))
                        {
                            IDictionary json = current as IDictionary;
                            object obj3 = convert(new JsonReader(json), type);
                            if (obj3 != null)
                            {
                                list.Add(obj3);
                            }
                        }
                        else if (!typeof(IList).IsAssignableFrom(current.GetType()))
                        {
                            if (typeof(JsonNumber).IsAssignableFrom(current.GetType()))
                            {
                                list.Add(((JsonNumber)current).ToInt64());
                                continue;
                            }
                            list.Add(current);
                        }
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
            }
            return list;

        }

    
        public object GetPrimitiveObject(object name)
        {
            return this.json[name];

        }

        public object GetReferenceObject(object name, Type type, DConvert convert)
        {
            IDictionary json = this.json[name] as IDictionary;
            if ((json != null) && (json.Count > 0))
            {
                return convert(new JsonReader(json), type);
            }
            return null;

        }


        public bool HasReturnField(object name)
        {
            return this.json.Contains(name);

        }
    }
}
