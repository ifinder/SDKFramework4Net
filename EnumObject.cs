﻿//==============================================================
//  Create by whl at 1/21/2015 10:11:38 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Cn.Finder.SDKFramework
{
    /// <summary>
    /// IComparer  无效
    /// 继承此类的类  是为了实现非整型枚举
    /// </summary>
    public  class  EnumObject<T>:IComparer,IComparable
    {

        protected List<T> values =new List<T>();

        protected  T value;

        /// <summary>
        /// 获取本身的值
        /// </summary>
        public  T Value
        {
            get { return this.value; }
        }

        /// <summary>
        /// 获取所有的值
        /// </summary>
        public List<T> Values
        {
            get
            {
                return this.values;
            }
        }
        public EnumObject(T value)
        {
            this.value = value;
            this.Values.Clear();
            this.Values.Add(value);
        }

        public EnumObject(EnumObject<T> item)
        {
            this.value = item.value;
            this.values = item.Values;
        }


        /// <summary>
        /// 把多个 值加入到集合
        /// </summary>
   //     public static T Default = default(T);

        public EnumObject<T> Or(EnumObject<T> item)
        {

            if (this.Value == null)
            {
                
                return (EnumObject<T>)item.MemberwiseClone();
            }
            else
            {
                EnumObject<T> newEnumObj = null;

               // newEnumObj = (EnumObject<T>)this.MemberwiseClone();

                MemoryStream stream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                newEnumObj=formatter.Deserialize(stream) as EnumObject<T>;

                newEnumObj.Values.AddRange(item.Values);
                return newEnumObj;
            }
            
        }


        /// <summary>
        /// 移除一条
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public EnumObject<T> Remove(EnumObject<T> item)
        {

            if (this.Value != null)
            {
                if (this.values.Contains(item.Value))
                {

                    this.Values.Remove(item.Value);
                }
            }
            return this;
        }


        /// <summary>
        /// 获取所有的值 逗号隔开
        /// </summary>
        /// <returns></returns>
        /// 
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (T item in Values)
            {
                sb.Append(",");
                sb.Append(item.ToString());
            }
            if(sb.ToString().Length>0)
                sb.Remove(0, 1);
            return sb.ToString();
        }


        public int Compare(object x, object y)
        {

            EnumObject<T> x_obj = x as EnumObject<T>;
            EnumObject<T> y_obj = y as EnumObject<T>;
            return x_obj.Value.ToString().CompareTo(y_obj.Value.ToString());

        }




        public int CompareTo(object obj)
        {
            EnumObject<T> x_obj = obj as EnumObject<T>;
            
            return this.Value.ToString().CompareTo(x_obj.Value.ToString());
        }



    }
}
