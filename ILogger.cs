﻿//==============================================================
//  Create by whl at 4/11/2014 11:03:14 AM.
//  Version 1.0
//  
//==============================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cn.Finder.SDKFramework
{
    public interface ILogger
    {
        // Methods
        void Error(string message);
        void Info(string message);
        void Warn(string message);

    }
}
